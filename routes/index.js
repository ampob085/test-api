const express = require('express');
const router = express.Router();
const testapi = require('./testapi.route')

router.use(testapi)

module.exports = router