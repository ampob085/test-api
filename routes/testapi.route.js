const express = require('express');
const router = express.Router();
const testapicontroller = require('../controller/testapi.controller')

router.get('/testapi', testapicontroller.testapi)

module.exports = router