require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` });
require('./config/database').connect();

const router = require("./routes/index");

const express = require('express');
const bp = require("body-parser")
const cors = require('cors')
const app = express()
 
app.use(cors())
const { API_PORT } = process.env;
const port = API_PORT || 4000;

app.use(bp.json())
app.use(router);

app.listen(port, () => {
    const test = (port == 4000) ? `URL=http://localhost:${port}/` : ''
    console.log(`Server running on mode ${process.env.NODE_ENV} at port ${port} ` + test );
})